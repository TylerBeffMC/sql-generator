﻿using System;

namespace GraphSecurityGenerator
{
    public class GraphAPIConfig
    {
        public int securityFunctionId;
        public string areaName; // Example: API Loan Session
        public string operationName; // Example: addAdjustment
        public GraphArea graphArea;
        public Guid GraphAreaId { get { return GraphAreas.graphAreaIds[graphArea]; } }
        public GraphQueryType graphQueryType;
        public Guid GraphQueryTypeId { get { return GraphQueryTypes.graphQueryTypeIds[graphQueryType]; } }
    }
}
