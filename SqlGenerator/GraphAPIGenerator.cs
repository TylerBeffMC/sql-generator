﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphSecurityGenerator
{
    public class GraphAPIGenerator
    {
        private GraphAPIConfig config;

        public GraphAPIGenerator(GraphAPIConfig config)
        {
            this.config = config;
        }

        public string GenerateSecurityFunctionSQL()
        {
            return $@"IF NOT EXISTS (SELECT sfunID FROM tbsSecurityFunction WHERE sfunID = {config.securityFunctionId})
BEGIN
	INSERT INTO tbsSecurityFunction (sfunID, sfunName, sfunDesc, sfunActiveLoan, WebFunction, sfunMODUid) 
	VALUES ({config.securityFunctionId}, '{config.areaName} {config.operationName}', 'self-descriptive', 0, 0, 'D66E3788-A97A-4C1F-8AB7-B12D1F994396')
END
ELSE
BEGIN
	UPDATE tbsSecurityFunction
	SET sfunName = '{config.areaName} {config.operationName}'
	WHERE sfunID = {config.securityFunctionId}
END
GO";
        }

        public string GenerateGroupSecurityFunctionSQL()
        {
            return $@"IF NOT EXISTS (SELECT 1 FROM dbo.tbmGroupSecFunction WHERE gsfnSFUNid = {config.securityFunctionId} AND gsfnGRUPid = 'D4865795-44A4-4A04-93A6-E251FF6F1720')
BEGIN
	INSERT INTO dbo.tbmGroupSecFunction (gsfnGRUPid, gsfnSFUNid, UpdatedByID, UpdatedDate)
	VALUES ('D4865795-44A4-4A04-93A6-E251FF6F1720', {config.securityFunctionId}, 'DCA6C8B5-BFA7-46D6-9628-41442B22D262', GETDATE())
END
GO";
        }

        public string GenerateGraphSecuritySQL()
        {
            var graphSecurityGuid = Guid.NewGuid();
            return $@"IF NOT EXISTS (SELECT 1 FROM dbo.[tbkGraphSecurity] WHERE [ID] = '{graphSecurityGuid}')
BEGIN
	INSERT INTO dbo.[tbkGraphSecurity] ([ID], [Description], [NoSecurity], [ModuleID], [SecurityFunctionID], [GraphAreaID], [GraphQueryTypeID], [GraphOperation], [UpdatedByID], [UpdatedDate])
	VALUES ('{graphSecurityGuid}', '{config.areaName} {config.operationName}', 0, NULL, {config.securityFunctionId}, '{config.GraphAreaId}', '{config.GraphQueryTypeId}', '{config.operationName}', NULL, NULL)
END
ELSE
BEGIN
	UPDATE dbo.tbkGraphSecurity SET [ID] = '{graphSecurityGuid}', [Description] = '{config.areaName} {config.operationName}', [NoSecurity] = 0, [ModuleID] = NULL, [SecurityFunctionID] = {config.securityFunctionId}, [GraphAreaID] = '{config.GraphAreaId}', [GraphQueryTypeID] = '{config.GraphQueryTypeId}', [GraphOperation] = '{config.operationName}', [UpdatedByID] = NULL, [UpdatedDate] = NULL WHERE [ID] = '{graphSecurityGuid}'
END
GO";
        }
    }
}
