﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphSecurityGenerator
{
    public enum GraphQueryType
    {
        Query,
        Mutation
    }

    public static class GraphQueryTypes
    {
        // TODO: Retrieve this data from the database instead
        public static Dictionary<GraphQueryType, Guid> graphQueryTypeIds = new Dictionary<GraphQueryType, Guid>()
        {
            { GraphQueryType.Query, Guid.Parse("D3DDF960-96EA-49C9-B769-1CA28BDF0F06") },
            { GraphQueryType.Mutation, Guid.Parse("E5B49380-292C-4A24-A22C-EF05AC7AD895") }
        };
    }
}
