﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphSecurityGenerator
{
    public enum GraphArea
    {
        LoanAttachments,
        FeatureStatus,
        LoanComments,
        LoanSession,
        Configuration,
        Pipeline,
        ProductsAndPricing,
        Clients,
        MonitoringHealthStatus,
        Tasks,
        Loans,
        LoanOverview,
        Security,
        FrontEndConfiguration,
        Documents
    }

    public static class GraphAreas
    {
        // TODO: Retrieve this data from the database instead
        public static Dictionary<GraphArea, Guid> graphAreaIds = new Dictionary<GraphArea, Guid>()
        {
            { GraphArea.LoanAttachments, Guid.Parse("F2661E1E-9B4C-4923-B44C-003A93DBB6A5") },
            { GraphArea.FeatureStatus, Guid.Parse("C1645C6A-3B28-4CF5-AEE8-18B18DD50605") },
            { GraphArea.LoanComments, Guid.Parse("F177E29E-373B-4C2A-83D6-220FA70868D8") },
            { GraphArea.LoanSession, Guid.Parse("5FACF822-A13E-4B0C-9C54-2346BD7DD17A") },
            { GraphArea.Configuration, Guid.Parse("42C6D8DE-A285-498C-AF99-26D84EE3F688") },
            { GraphArea.Pipeline, Guid.Parse("03849B65-5547-4DF8-81A5-389B2FAB8AEC") },
            { GraphArea.ProductsAndPricing, Guid.Parse("C1D5089C-FA46-42EE-ABDA-3C48DEB92292") },
            { GraphArea.Clients, Guid.Parse("8508B9EE-3531-4AD8-9695-3E655B460D0E") },
            { GraphArea.MonitoringHealthStatus, Guid.Parse("FB769B83-3919-4C87-9FD0-44E23E35E94B") },
            { GraphArea.Tasks, Guid.Parse("C2C4F232-0B01-44CD-936A-44F41B488C70") },
            { GraphArea.Loans, Guid.Parse("06778B04-A0CF-40CA-A9D4-4DEDEB97130C") },
            { GraphArea.LoanOverview, Guid.Parse("F8D985F0-2877-4012-BA8D-754976F7FAE6") },
            { GraphArea.Security, Guid.Parse("5FA55A4A-8ACC-4F19-937A-8C93B6F8EDB6") },
            { GraphArea.FrontEndConfiguration, Guid.Parse("BA14FFA7-9CC5-4239-ABC2-CB1CD88C79A7") },
            { GraphArea.Documents, Guid.Parse("378823DF-1C05-466F-9139-FB9A68254B55") }
        };
    }
}
