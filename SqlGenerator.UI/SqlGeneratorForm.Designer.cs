﻿namespace GraphSecurityGenerator.UI
{
    partial class SqlGeneratorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.generatorModeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.configPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.generateSqlButton = new System.Windows.Forms.Button();
            this.sqlTxtBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // generatorModeComboBox
            // 
            this.generatorModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.generatorModeComboBox.FormattingEnabled = true;
            this.generatorModeComboBox.Location = new System.Drawing.Point(148, 12);
            this.generatorModeComboBox.Name = "generatorModeComboBox";
            this.generatorModeComboBox.Size = new System.Drawing.Size(121, 28);
            this.generatorModeComboBox.TabIndex = 0;
            this.generatorModeComboBox.SelectedIndexChanged += new System.EventHandler(this.generatorModeComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Generator Mode:";
            // 
            // configPanel
            // 
            this.configPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.configPanel.Location = new System.Drawing.Point(6, 25);
            this.configPanel.Name = "configPanel";
            this.configPanel.Size = new System.Drawing.Size(400, 395);
            this.configPanel.TabIndex = 2;
            // 
            // generateSqlButton
            // 
            this.generateSqlButton.Location = new System.Drawing.Point(6, 25);
            this.generateSqlButton.Name = "generateSqlButton";
            this.generateSqlButton.Size = new System.Drawing.Size(126, 32);
            this.generateSqlButton.TabIndex = 3;
            this.generateSqlButton.Text = "Generate SQL";
            this.generateSqlButton.UseVisualStyleBackColor = true;
            this.generateSqlButton.Click += new System.EventHandler(this.generateSqlButton_Click);
            // 
            // sqlTxtBox
            // 
            this.sqlTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sqlTxtBox.Location = new System.Drawing.Point(6, 63);
            this.sqlTxtBox.Multiline = true;
            this.sqlTxtBox.Name = "sqlTxtBox";
            this.sqlTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.sqlTxtBox.Size = new System.Drawing.Size(538, 349);
            this.sqlTxtBox.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.configPanel);
            this.groupBox1.Location = new System.Drawing.Point(16, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 432);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Config";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.generateSqlButton);
            this.groupBox2.Controls.Add(this.sqlTxtBox);
            this.groupBox2.Location = new System.Drawing.Point(436, 53);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(552, 432);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output";
            // 
            // SqlGeneratorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 502);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.generatorModeComboBox);
            this.Name = "SqlGeneratorForm";
            this.Text = "SQL Generator";
            this.Load += new System.EventHandler(this.SqlGeneratorForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox generatorModeComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel configPanel;
        private System.Windows.Forms.Button generateSqlButton;
        private System.Windows.Forms.TextBox sqlTxtBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

