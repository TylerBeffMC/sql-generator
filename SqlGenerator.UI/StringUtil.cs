﻿using System.Text;

namespace GraphSecurityGenerator.UI
{
    public static class StringUtil
    {
		public static string FormatCamelCase(string str)
		{
			var sb = new StringBuilder(str.Length);
			sb.Append(char.ToUpper(str[0]));
			for (var i = 1; i < str.Length; i++)
			{
				if (char.IsUpper(str[i]) && char.IsLower(str[i - 1]) || char.IsNumber(str[i]))
					sb.Append(' ');

				sb.Append(str[i]);
			}

			return sb.ToString();
		}
	}
}
