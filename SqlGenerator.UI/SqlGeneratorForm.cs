﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphSecurityGenerator.UI
{
    public partial class SqlGeneratorForm : Form
    {
        public SqlGeneratorForm()
        {
            InitializeComponent();
        }

        private void SqlGeneratorForm_Load(object sender, EventArgs e)
        {
            generatorModeComboBox.DataSource = Enum.GetValues(typeof(GeneratorMode));
        }

        private void generatorModeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var generatorMode = (GeneratorMode)generatorModeComboBox.SelectedIndex;
            switch (generatorMode)
            {
                case GeneratorMode.GraphAPI:
                    GenerateGraphApiControls();
                    break;
                default:
                    break;
            }
        }

        private void GenerateGraphApiControls()
        {
            var fields = typeof(GraphAPIConfig).GetFields(BindingFlags.Public | BindingFlags.Instance);
            for (var i = 0; i < fields.Length; i++)
            {
                var field = fields[i];

                var label = new Label();
                label.Width = 120;
                label.Text = StringUtil.FormatCamelCase(field.Name);
                configPanel.Controls.Add(label);

                var control = UIGenerator.CreateControlForType(field.FieldType);
                configPanel.Controls.Add(control);
            }
        }

        private void generateSqlButton_Click(object sender, EventArgs e)
        {
            var generatorMode = (GeneratorMode)generatorModeComboBox.SelectedIndex;
            switch (generatorMode)
            {
                case GeneratorMode.GraphAPI:
                    GenerateGraphApiSql();
                    break;
                default:
                    break;
            }
        }

        private void GenerateGraphApiSql()
        {
            var fields = typeof(GraphAPIConfig).GetFields(BindingFlags.Public | BindingFlags.Instance);
            var controls = configPanel.Controls;
            var config = new GraphAPIConfig();

            for (var i = 0; i < fields.Length; i++)
            {
                var field = fields[i];
                var control = controls[i * 2 + 1];
                var value = UIGenerator.GetValueForControl(control, field.FieldType);
                field.SetValue(config, value);
            }

            var sql = GenerateSql(config);
            sqlTxtBox.Text = sql;
        }

        private string GenerateSql(GraphAPIConfig config)
        {
            var sb = new StringBuilder();
            var generator = new GraphAPIGenerator(config);

            sb.AppendLine("tbsSecurityFunction.sql:");
            var securityFunctionSql = generator.GenerateSecurityFunctionSQL();
            sb.AppendLine(securityFunctionSql);
            sb.AppendLine();

            sb.AppendLine("tbmGroupSecFunction.sql:");
            var groupSecurityFunctionSql = generator.GenerateGroupSecurityFunctionSQL();
            sb.AppendLine(groupSecurityFunctionSql);
            sb.AppendLine();

            sb.AppendLine("tbkGraphSecurity.sql:");
            var graphSecuritySql = generator.GenerateGraphSecuritySQL();
            sb.AppendLine(graphSecuritySql);

            return sb.ToString();
        }
    }
}
