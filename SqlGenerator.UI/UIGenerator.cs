﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphSecurityGenerator.UI
{
    public static class UIGenerator
    {
        public static Control CreateControlForType(Type type)
        {
            if (type.IsEnum)
            {
                var comboBox = new ComboBox();
                comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
                comboBox.DataSource = type.GetEnumValues();
                return comboBox;
            }

            switch (type.Name)
            {
                case "Int32":
                case "String":
                    return new TextBox();
                default:
                    throw new NotSupportedException($"{type.Name} is not supported.");
            }
        }

        public static object GetValueForControl(Control control, Type type)
        {
            if (type.IsEnum)
            {
                var comboBox = (ComboBox)control;
                var selectedIndex = comboBox.SelectedIndex;
                var enumValues = type.GetEnumValues();

                return enumValues.GetValue(selectedIndex);
            }

            switch (type.Name)
            {
                case "Int32":
                    {
                        if (int.TryParse(((TextBox)control).Text, out var value))
                            return value;

                        return 0;
                    }
                case "String":
                    return ((TextBox)control).Text;
                default:
                    throw new NotSupportedException($"{type.Name} is not supported.");
            }
        }
    }
}
