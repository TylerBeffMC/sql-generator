Use [ID Generator](https://idgenerator.mortgagecadence.com) to generate a security function ID.

Files to update:

* \DatabaseProjects\ELC\Cadence\Reference Data\ReleaseData\<version_number>\tbkGraphSecurity.sql
* \DatabaseProjects\ELC\Cadence\Reference Data\ReleaseData\<version_number>\tbsSecurityFunction.sql
* \DatabaseProjects\ELC\Cadence\Reference Data\ReleaseData\<version_number>\tbmGroupSecFunction.sql

![Program](/Images/program.png)