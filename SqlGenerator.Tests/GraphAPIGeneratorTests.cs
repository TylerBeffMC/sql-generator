﻿using NUnit.Framework;
using GraphSecurityGenerator;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphSecurityGenerator.Tests
{
    [TestFixture]
    public class GraphAPIGeneratorTests
    {
        [Test]
        public void GeneratorTest()
        {
            var config = GetConfig();
            var generator = new GraphAPIGenerator(config);
            var securityFunctionSql = generator.GenerateSecurityFunctionSQL();
            var groupSecurityFunctionSql = generator.GenerateGroupSecurityFunctionSQL();
            var graphSecuritySql = generator.GenerateGraphSecuritySQL();

            Assert.IsNotEmpty(securityFunctionSql);
            Assert.IsNotEmpty(groupSecurityFunctionSql);
            Assert.IsNotEmpty(graphSecuritySql);
        }

        private static GraphAPIConfig GetConfig()
        {
            return new GraphAPIConfig()
            {
                securityFunctionId = 19909,
                areaName = "API Loan Session",
                operationName = "deleteAdjustment",
                graphArea = GraphArea.LoanSession,
                graphQueryType = GraphQueryType.Mutation
            };
        }
    }
}